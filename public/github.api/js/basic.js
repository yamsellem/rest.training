"use strict";

$(function() {

	var token = 'place your token here';

	var _isObject = function(obj) {
		var type = typeof obj;
		return type === 'function' || type === 'object' && !!obj;
	};

	var display = function(data) {
		$('.hero-content code').text(_isObject(data) ? JSON.stringify(data, undefined, 2) : data);
	};

	var fetch = function() {
		display('loading');

		$.ajax({
			url: 'https://api.github.com/',
			headers: { 'Authorization': 'token ' + token }
		}).done(display);
	};

	fetch();
	$('input[type=submit]').on('click', fetch);
});