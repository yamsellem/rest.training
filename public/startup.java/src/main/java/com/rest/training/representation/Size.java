package com.rest.training.representation;

public enum Size {
    S, M, L, XL
}
