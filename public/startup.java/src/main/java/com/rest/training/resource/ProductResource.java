package com.rest.training.resource;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.rest.training.data.Products;
import com.rest.training.representation.Product;

@Path("product")
public class ProductResource {
    @GET
    @Path("{id}")
    public Product get(@PathParam("id") long id) {
        return Products.get(id);
    }

    @POST
    public Product post(Product product) {
        Products.put(product);
        return product;
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") long id) {
        Products.delete(id);
    }

    @GET
    public Response get() {
        List<Product> products = Products.get();
        GenericEntity<List<Product>> entity = new GenericEntity<List<Product>>(products) {};
        return Response.ok(entity).build();
    }
}
