package com.rest.training;

import org.eclipse.jetty.webapp.WebAppContext;

public class ProductServer {

    public static void main(String [] args) throws Throwable {
        org.eclipse.jetty.server.Server server;
        server = new org.eclipse.jetty.server.Server(3000);
        server.setHandler(new WebAppContext("src/main/webapp", "/"));
        server.start();
    }
}
