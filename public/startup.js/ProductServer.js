"use strict";

var express = require('express'),
	bodyParser = require('body-parser'),
	_ = require('lodash'),
	app = express(),
	router = express.Router();

router.get('/', function(req, res) {
	res.send({});
});

var products = [];

router.get('/product', function(req, res) {
	res.send(products);
});

router.post('/product', function(req, res) {
	var product = _.clone(req.body);
	product.id = products.length + 1;
	products.push(product);
	res.send(product);
});

router.delete('/product/:id', function(req, res) {
	products = _.reject(products, {id: parseInt(req.params.id)});
	res.send({});
});

app
	.use(bodyParser.json({limit: '70mb'}))
	.use(router)
	.listen(3000);

console.log('server is listening at 3000');