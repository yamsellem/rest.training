"use strict";

var _ = require('lodash'),
	Promise = require('bluebird'),
	rp = require('request-promise').defaults({ timeout: 2000, jar: true }),
	socketioJwt = require("socketio-jwt"),
	secret = require('./config').secret;

var Checker = function() {
	this.clients = {};
	this.names = {};
};

Checker.prototype = Object.create({
	defaults: {
		'server.up': false,
		'shoe.list': false,
		'shoe.create': false,
		'shoe.update': false,
		'shoe.delete': false,
		'customer.create': false,
		'basket.add': false,
		'basket.low': false,
		'secure.unnamed': false,
		'secure.signin': false,
		'secure.signed': false,
		'secure.signout': false
	},
	loop: function(io) {
		this.io = io;
		io.use(socketioJwt.authorize({
			secret: secret,
			handshake: true
		}));

		this.io.on('connection', function (socket) {
			this.names[socket.handshake.address] = {
				socket: socket,
				name: socket.decoded_token.name
			};
			this.clients[socket.handshake.address] = _.clone(this.defaults);
			
			socket.on('disconnect', function(socket) {
				if(socket.handshake && socket.handshake.address) {
					delete this.names[socket.handshake.address];
					delete this.clients[socket.handshake.address];
				}
			}.bind(this));
		}.bind(this));


		setInterval(this.pulseAll.bind(this), 1000);
	},
	pulse: function(ip) {
		this.generateColors();

		var promises = [];
		promises.push(this.originServer(ip));
		promises.push(this.shoeList(ip));
		promises.push(this.shoeCreate(ip));
		promises.push(this.shoeUpdate(ip));
		promises.push(this.shoeDelete(ip));
		promises.push(this.customerCreate(ip));

		return Promise.settle(promises)
			.bind(this)
			// force sequential execution
			.then(function() { return this.basketAdd(ip); })
			.then(function() { return this.basketCannotAdd(ip); })
			.then(function() { return this.secureAccessAnonymous(ip); })
			.then(function() { return this.secureSignin(ip); })
			.then(function() { return this.secureAccessSigned(ip); })
			.then(function() { return this.secureSignout(ip); })
			.then(function() {
				this.names[ip].socket.emit('change:pulse', this.clients[ip]);
			});
	},
	pulseAll: function() {
		var promises = [];

		_.each(this.clients, function(flags, ip) {
			promises.push(this.pulse(ip));
		}.bind(this));

		Promise.settle(promises).bind(this).then(function() {
			var race = [];

			_.each(this.clients, function(flags, ip) {
				var name = this.names[ip].name;

				var score = _.reduce(flags, function(memo, flag) {
					memo.of++;
					if(flag) memo.done++;
					return memo;
				}, {done: 0, of: 0});

				race.push({name: name, score: score});
			}.bind(this));

			this.io.emit('change:race', _.sortBy(race, 'name'));
		});
	},
	// scenarii
	originServer: function(ip) {
		return this.request('/', 'GET', ip)
		.then(function(result) {
			this.clients[ip]['server.up'] = result.checked;
		})
		.catch(function() {
			this.clients[ip]['server.up'] = false;
		});
	},
	shoeList: function(ip) {
		return this.request('/shoe', 'GET', ip, function(shoes) {
			return _.isArray(shoes);
		})
		.then(function(result) {
			this.clients[ip]['shoe.list'] = result.checked;
		})
		.catch(function() {
			this.clients[ip]['shoe.list'] = false;
		});
	},
	shoeCreate: function(ip) {
		var body = {name: this.getName(), quantity: 2, price: 12};

		return this._createShoe(ip, body)
		.then(function() {
			return this.request('/shoe', 'GET', ip, function(shoes) {
				return this.itemsMatch(shoes, 'id', 'name', 'quantity', 'price')
					&& this.itemsContains(shoes, body);
			});
		})
		.then(function(result) {
			this.clients[ip]['shoe.create'] = result.checked;
		})
		.catch(function() {
			this.clients[ip]['shoe.create'] = false;
		});
	},
	shoeUpdate: function(ip) {
		var body = {name: this.getName(), quantity: 2, price: 12};

		return this._createShoe(ip, body)
		.then(function(result) {
			body.id = result.body.id;
			body.quantity = 7;

			return this.request('/shoe/' + body.id, 'PUT', ip, body, function(shoe) {
				return this.itemMatch(shoe, 'id', 'name', 'quantity', 'price')
					&& this.itemIs(shoe, body);
			});	
		})
		.then(function(result) {
			if(!result.checked) return Promise.reject();

			return this.request('/shoe', 'GET', ip, function(shoes) {
				return this.itemsMatch(shoes, 'id', 'name', 'quantity', 'price')
					&& this.itemsContains(shoes, result.body);
			});
		})
		.then(function(result) {
			this.clients[ip]['shoe.update'] = result.checked;
		})
		.catch(function() {
			this.clients[ip]['shoe.update'] = false;
		});
	},
	shoeDelete: function(ip) {
		var body = {name: this.getName(), quantity: 2, price: 12};

		return this._createShoe(ip, body)
		.then(function(result) {
			body.id = result.body.id;

			return this.request('/shoe/' + body.id, 'DELETE', ip, function(shoe) {
				return _.isObject(shoe) && _.isEmpty(shoe);
			});
		})
		.then(function(result) {
			if(!result.checked) return Promise.reject();

			return this.request('/shoe', 'GET', ip, function(shoes) {
				return !this.itemsContains(shoes, result.body);
			});
		})
		.then(function(result) {
			this.clients[ip]['shoe.delete'] = result.checked;
		})
		.catch(function() {
			this.clients[ip]['shoe.delete'] = false;
		});
	},
	customerCreate: function(ip) {
		var body = {name: 'Paul'};

		return this._createCustomer(ip, body)
		.then(function() {
			return this.request('/customer', 'GET', ip, function(customers) {
				return this.itemsMatch(customers, 'id', 'name')
					&& this.itemsContains(customers, body);
			});
		})
		.then(function(result) {
			this.clients[ip]['customer.create'] = result.checked;
		})
		.catch(function() {
			this.clients[ip]['customer.create'] = false;
		});
	},
	basketAdd: function(ip) {
		var paul = {name: 'Paul'},
			stored = {name: this.getName(), quantity: 5, price: 12},
			booked = {},
			customerId = 0;

		return this._createShoe(ip, stored)
		.then(function(result) {
			booked = _.clone(result.body);

			return this._createCustomer(ip, paul);
		})
		.then(function(result) {
			customerId = result.body.id;
			booked.quantity = 2;

			return this.request('/customer/' + customerId + '/basket', 'POST', ip, booked, function(basket) {
				return this.itemMatch(basket, 'price', 'shoes')
					&& this.itemIs(basket, {price: 24, shoes: [booked]});
			});
		})
		.then(function() {
			return this.request('/customer/' + customerId + '/basket', 'GET', ip, function(basket) {
				return this.itemMatch(basket, 'price', 'shoes')
					&& this.itemIs(basket, {price: 24, shoes: [booked]});
			});
		})
		.then(function(result) {
			this.clients[ip]['basket.add'] = result.checked;
		})
		.catch(function() {
			this.clients[ip]['basket.add'] = false;
		});
	},
	basketCannotAdd: function(ip) {
		var paul = {name: 'John'},
			stored = {name: this.getName(), quantity: 5, price: 12},
			booked = {},
			customerId = 0;

		return this._createShoe(ip, stored)
		.then(function(result) {
			booked = _.clone(result.body);

			return this._createCustomer(ip, paul);
		})
		.then(function(result) {
			customerId = result.body.id;
			booked.quantity = 9;

			return this.request('/customer/' + customerId + '/basket', 'POST', ip, booked);
		})
		.then(function(result) {
			this.clients[ip]['basket.low'] = false;
		})
		.catch(function(reason) {
			this.clients[ip]['basket.low'] = (reason.statusCode === 412);
		});
	},
	secureAccessAnonymous: function(ip) {
		return this.request('/user/signin', 'DELETE', ip)
		.catch(_.noop)
		.then(function() {
			return this.request('/secure', 'GET', ip)
		})
		.then(function(result) {
			this.clients[ip]['secure.unnamed'] = false;
		})
		.catch(function(reason) {
			this.clients[ip]['secure.unnamed'] = (reason.statusCode === 401);
		});
	},
	secureSignin: function(ip) {
		return this.request('/user/signin', 'DELETE', ip)
		.catch(_.noop)
		.then(function() {
			return this.request('/user/signin', 'POST', ip, {user: 'Paul', pass: 'Submarine'})
		})
		.then(function(result) {
			this.clients[ip]['secure.signin'] = result.checked;
		})
		.catch(function() {
			this.clients[ip]['secure.signin'] = false;
		});
	},
	secureAccessSigned: function(ip) {
		return this.request('/user/signin', 'DELETE', ip)
		.catch(_.noop)
		.then(function() {
			return this.request('/user/signin', 'POST', ip, {user: 'Paul', pass: 'Submarine'})
		})
		.then(function(result) {
			return this.request('/secure', 'GET', ip, function(secured) {
				return this.itemMatch(secured, 'secret')
					&& this.itemIs(secured, {secret: 'Yellow'});
			});
		})
		.then(function(result) {
			this.clients[ip]['secure.signed'] = result.checked;
		})
		.catch(function(reason) {
			this.clients[ip]['secure.signed'] = false;
		});
	},
	secureSignout: function(ip) {
		return this.request('/user/signin', 'DELETE', ip)
		.catch(_.noop)
		.then(function() {
			return this.request('/user/signin', 'POST', ip, {user: 'Paul', pass: 'Submarine'})
		})
		.then(function(result) {
			return this.request('/secure', 'GET', ip, function(secured) {
				return this.itemMatch(secured, 'secret')
					&& this.itemIs(secured, {secret: 'Yellow'});
			});
		})
		.then(function() {
			return this.request('/user/signin', 'DELETE', ip)
		})
		.then(function() {
			return this.request('/secure', 'GET', ip);
		})
		.then(function(result) {
			this.clients[ip]['secure.signout'] = false;
		})
		.catch(function(reason) {
			this.clients[ip]['secure.signout'] = (reason.statusCode === 401);
		});
	},
	// creation helpers
	_create: function(ip, path, body, fields) {
		return this.request(path, 'GET', ip, function(shoes) {
			return this.itemsMatch(shoes, fields)
				&& this.itemsContains(shoes, body);
		})
		.then(function(result) {
			if(result.checked) {
				return Promise.resolve({checked: result.checked, body: _.findWhere(result.body, body)});
			} else {
				return this.request(path, 'POST', ip, body, function(shoe) {
					return this.itemMatch(shoe, fields)
						&& this.itemIs(shoe, body);
				})
			}
		});
	},
	_createShoe: function(ip, body) {
		return this._create(ip, '/shoe', body, ['id', 'name', 'quantity']);
	},
	_createCustomer: function(ip, body) {
		return this._create(ip, '/customer', body, ['id', 'name']);
	},
	// asserters
	itemMatch: function(item, fields) {
		var properties = (fields && _.isArray(fields)) ? fields : _.rest(arguments);
		var valid = true;

		_.each(properties, function(property) {
			valid = valid && _.has(item, property);	
		});
		return valid;
	},
	itemsMatch: function(items, fields) {
		var properties = (fields && _.isArray(fields)) ? fields : _.rest(arguments);
		var valid = _.isArray(items) && !_.isEmpty(items);
		
		_.each(items, function(item) {
			this.itemMatch(item, properties);
		}.bind(this));
		return valid;
	},
	itemIs: function(item, clone) {
		return this.itemsContains([item], clone);
	},
	itemsContains: function(items, item) {
		return !_.isEmpty(_.where(items, item));
	},
	// helpers
	request: function(uri, method, ip, body, check) {
		var base = 'http://' + ip + ':3000';
		if(_.isFunction(body) || (!body && !check)) {
			check = body;
			body = true;
		}

		return rp({uri: base + uri, method: method, json: body})
			.then(function(body) {
				var checked = _.isFunction(check) ? check.call(this, body) : true;
				return Promise.resolve({checked: checked, body: body});
			}.bind(this))
			.catch(function(reason) {
				return Promise.reject(reason);
			})
			.bind(this);
	},
	// getters
	getName: function() {
		return 'Boat Boots ' + this.getColor();
	},
	getColor: function() {
		var index = _.random(0, this.colors.length-1);
		var color = this.colors[index];
		this.colors.splice(index, 1);
		return color;
	},
	generateColors: function() {
		this.colors = ['aqua','azure','beige','black','blue','brown','cyan','darkblue','darkcyan','darkgrey','darkgreen','darkkhaki','darkmagenta','darkolivegreen','darkorange','darkorchid','darkred','darksalmon','darkviolet','fuchsia','gold','green','indigo','khaki','lightblue','lightcyan','lightgreen','lightgrey','lightpink','lightyellow','lime','magenta','maroon','navy','olive','orange','pink','purple','violet','red','silver','white','yellow'];
	}
});

module.exports = new Checker();