"use strict";

$(function() {
	// foundation
	$(document).foundation();
	// auth
	var _sync = Backbone.sync;
	Backbone.sync = function(method, model, options) {
	  options.beforeSend = function(xhr) {
	  	var token = $.localStorage.getItem('token');
	    return xhr.setRequestHeader('Authorization', 'Bearer ' + token);
	  };
	  return _sync.call(this, method, model, options);
	};
	// !auth

	var User = Backbone.Model.extend({
		url: '/user'
	});
	var user = new User();

	var pulse = new Backbone.Model({});
	var race = new Backbone.Model({});

	var PulseView = Backbone.View.extend({
		el: '.js-pulse',
		template: Handlebars.compile($("#pulse-template").html()),
		initialize: function() {
			this.render();
			this.listenTo(pulse, 'change', this.render);
		},
		render: function() {
			$('.tooltip').remove();

			var board = _.clone(pulse.attributes);
			board = _.mapValues(board, function(value, key) {
				var help = '';

				if(key === 'server.up') help = 'A server should respond to / on port 3000';
				if(key === 'shoe.list') help = 'GET /shoe should return an array of [id, name, quantity, price]';
				if(key === 'shoe.create') help = 'POST /shoe should create a shoe and GET /shoe should return all of them, including it';
				if(key === 'shoe.update') help = 'PUT /shoe/:id should update a shoe and GET /shoe should return all of them, including it';
				if(key === 'shoe.delete') help = 'DELETE /shoe/:id should delete a shoe';
				if(key === 'customer.create') help = 'POST /customer should create a customer and GET /customer should return all of them, including it';
				if(key === 'basket.add') help = 'POST /customer/:id/basket should add a shoe {id,name,quantity,price} to the basket and compute the total price (quantity*price). The basket look should like {price, shoes:[]}';
				if(key === 'basket.low') help = 'POST /customer/:id/basket should not add a shoe quantity upper to the stock and should return 412 instead';
				if(key === 'secure.unnamed') help = 'GET /secure should return 401 unauthorised';
				if(key === 'secure.signin') help = 'POST /user/signin {user: "Paul", pass: "Submarine"} should return 200 with a cookie';
				if(key === 'secure.signed') help = 'After login, GET /secure should return 200 with {secret: "Yellow"}';
				if(key === 'secure.signout') help = 'DELETE /user/login should logout user';

				return {state: value, help: help};
			});
			this.$el.html(this.template(board));

			$(document).foundation('tooltip', 'reflow');
		}
	});

	var pulseView = new PulseView();

	var RaceView = Backbone.View.extend({
		el: '.js-race',
		template: Handlebars.compile($("#race-template").html()),
		initialize: function() {
			this.render();
			this.listenTo(race, 'change', this.render);
		},
		render: function() {
			var tracks = _.clone(race.attributes);
			if(_.isEmpty(tracks)) return;
			
			_.each(tracks, function(track) {
				track.progress = [];
				for(var i=0; i<track.score.of; i++) {
					track.progress.push(track.score.done > i);
				}
			});

			this.$el.html(this.template(tracks));
		}
	});

	var raceView = new RaceView();

	var Form = Backbone.View.extend({
		el: '.about-content',
		events: {
			submit: 'submit'
		},
		initialize: function() {
			this.render();

			var name = $.localStorage.getItem('name');
			if(name)
				this.initializeSocket();
		},
		initializeSocket: function() {
			var token = $.localStorage.getItem('token');

			this.socket = io('/', {forceNew: true, query: 'token=' + token});
			this.socket.on('change:pulse', function(value) {
				pulse.set(value);
			});
			this.socket.on('change:race', function(value) {
				race.set(value);
			});
		},
		render: function() {
			var name = $.localStorage.getItem('name');
			if(!_.isEmpty(name)) {
				this.$('.js-register').hide();
				this.$('.js-registred').show();
				this.$('h4').text(name + ', good luck to complete those steps.');
				pulseView.$el.show();
				raceView.$el.show();
			} else {
				this.$('.js-register').show();
				this.$('.js-registred').hide();
				this.$('h4').text('The stage is set, let the green flag drops!');
				pulseView.$el.hide();
				raceView.$el.hide();
			}
		},
		submit: function(e) {
			e.preventDefault();
			this.$('input[type=submit]').removeClass('animated shake');

			var token = $.localStorage.getItem('token');
			if(_.isEmpty(token))
				this.login();
			else
				this.logout();
		},
		login: function() {
			var name = this.$('input[type=text]').val();
			if(_.isEmpty(name)) {
				_.delay(_.bind(function() {
					this.$('input[type=submit]').addClass('animated shake');
				}, this));
			} else {
				user
					.save({name: name})
					.done(_.bind(function() {
						$.localStorage.setItem('name', user.get('name'));
						$.localStorage.setItem('token', user.get('token'));
						this.render();
						this.initializeSocket();
					}, this))
					.fail(_.bind(function() {
						this.$('input[type=submit]').addClass('animated shake');
					}, this));
			}
		},
		logout: function() {
			if (confirm('Are you sure you want to leave?')) {
				user
					.destroy({success: _.bind(function() {
						$.localStorage.removeItem('name');
						$.localStorage.removeItem('token');
						this.socket.disconnect();
						this.render();
						user.clear();
					}, this)});
			}
		}
	});

	var form = new Form();
});