REST Training, shoo-in
======================

Resources
---------
We've just met Paul, happy owner of a local shoe store. Paul needs our help to design his new online store. First of all, he needs to handle his catalog, adding & removing shoe at will. Once a shoe is in the catalog, he may update its stock when he sales one pair.

*Which resources does he need and how will he operate them?*

/shoe GET,POST {name, quantity, price}
/shoe/{id} PUT,DELETE

Fine, Paul got's a catalog. His next wish is to track customer purchases. When a customer buy a pair of shoes, it has to be recorded, and the stock has to be updated. 

*Which resources does he need and how will he operate them?*

/customer/{id}/purchase POST {shoes}

Fine, each customer get a purchases history. Paul's next wish is to reward his loyal customers, offering them a 20% discount after 3 purchases. When a customer is interested in buying several articles, Paul can then tell him/her the price he/she will have to pay and if he/she benefits from a discount.

*Which resources does he need and how will he operate them?*

/customer/{id}/basket GET,PUT {shoes, discount, price}
/customer/{id}/basket/buy POST

Fine, Paul is delighted by all his happy customers. Then a customer comes back and wants to swap his last purchase: the shoe size doesn't fit.

*Which resources does he need and how will he operate them?*

/customer/{id}/purchase/{id} DELETE 
/customer/{id}/basket GET {discount, price}

Fine, fine, fine. Paul's next wish is to have a purchases history for all his customers.

*Which resources does he need and how will he operate them?*

/customer/history GET [{id,history:[shoes,date]}]

Querying
--------
Winter is coming. Paul would like to offer a 30% discount to his 5 best customers (with highest total charges).

*Which resources does he need and how will he operate them?*

/customer?sort='best'&limit=5
/customer/reward?id[]=12&id[]=24

Paul has been busy lately, his shoes has encountered a tremendous success. First he thanks you very much. Then, he remembered this old wish he had, knowing the shoes low on stock (0, 1 or 2 pairs) to order them before they cease to be available.

*Which resources does he need and how will he operate them?*

/shoe?quantity[]=0&quantity[]=1&best-selling=true

Hypermedia
----------
In his shower, Paul had this brilliant idea: when a customer comes back to his store, he can be suggested 2 designs related to his/her previous purchases. The problem his, Paul handles a suggestion list himself every morning and the computation for a targeted client can be slow (~10 seconds).

*Which resources does he need and how will he operate them?*

/customer/{id}/suggestion GET [{date,shoes},{..}]
/customer/{id}/suggestion/compute POST 202 {link: /suggestion/{id}}
/customer/{id}/suggestion/{id} GET polling {state: pending}
/customer/{id}/suggestion/{id} GET polling {state: done, shoes: []}

Paul is keen on feedback. He has created a survey and offers 10% to every customer who fill it. It is composed of several steps but, those have to depends on the last purchases of the customer whose filling the form. Maybe the second step won't concern him/her.

*Which resources does he need and how will he operate them?*

/customer/{id}/survey GET {questions, link: /survey/step/1, done: false}
/customer/{id}/survey/step/1 POST {questions, answers} -> {questions, link: /survey/step/2, done: false}
/customer/{id}/survey/step/2 POST {questions, answers} -> {questions, link: /survey/step/5, done: false}
/customer/{id}/survey GET {questions, link: /survey/step/5, done: false}
/customer/{id}/survey/step/5 POST {questions, answers} -> {done: true}
/customer/{id}/survey GET {done: true}

User access
-----------
Paul is a bit scared that anyone can access all his customer datas. He wish he add a way to create two accounts, one for his partner, Linda, and one for himself. And a way to know if it's he or she who's signed in and a way to sign out.

*Which resources does he need and how will he operate them?*

Github Public Api
-----------------

Before anything, generate an [oauth token](https://help.github.com/articles/creating-an-access-token-for-command-line-use/) with a github account. Then, git clone (or download) the public folder of this repository. Bower install & display index.html. Edit basic.js to access GitHub Public Api and here is a list of mysterious questions:

What is the creation date of substack's node-browserify library?
https://api.github.com/search/repositories?q=browserify
-> 2010-09-22 16:11:32

How many comments get its first issue of all time?
https://api.github.com/repos/substack/node-browserify/issues?state=all&sort=created&direction=asc
-> 2

How can you star & unstar the project with your account without using the ui?
https://api.github.com/user/starred/substack/node-browserify

Does the last pull request author already commited in the project?
https://api.github.com/repos/substack/node-browserify/pulls
https://api.github.com/repos/substack/node-browserify/commits?author=piii
-> no

To finish, create a gist containing two text files.
https://api.github.com/gists