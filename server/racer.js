"use strict";

var express = require('express'),
	bodyParser = require('body-parser'),
	session = require('express-session'),
	_ = require('lodash'),
	app = express(),
	router = express.Router();

router.get('/', function(req, res) {
	res.send({});
});

var shoes = [];
var customers = [];
var basket = {};

router.get('/shoe', function(req, res) {
	res.send(shoes);
});

router.post('/shoe', function(req, res) {
	var shoe = _.clone(req.body);
	shoe.id = shoes.length + 1;
	shoes.push(shoe);
	res.send(shoe);
});

router.put('/shoe/:id', function(req, res) {
	var shoe = _.findWhere(shoes, {id: parseInt(req.params.id)});
	if(!shoe) return res.send({});

	shoe.name = req.body.name;
	shoe.quantity = req.body.quantity;
	res.send(shoe);
});

router.delete('/shoe/:id', function(req, res) {
	shoes = _.reject(shoes, {id: parseInt(req.params.id)});
	res.send({});
});

router.get('/customer', function(req, res) {
	res.send(customers);
});

router.post('/customer', function(req, res) {
	var customer = _.clone(req.body);
	customer.id = customers.length + 1;
	customers.push(customer);
	res.send(customer);
});

router.get('/customer/:id/basket', function(req, res) {
	var id = req.params.id;
	if(!basket[id])
		return res.status(204).send();

	res.send(basket[id]);
});

router.post('/customer/:id/basket', function(req, res) {
	var id = req.params.id;
	if(!basket[id])
		basket[id] = {};

	var shoe = _.find(shoes, {id: req.body.id});
	if(shoe && shoe.quantity < req.body.quantity)
		return res.status(412).send({});

	basket[id].price = 0;
	basket[id].shoes = [];

	basket[id].shoes.push(req.body);
	_.each(basket[id].shoes, function(shoe) {
		basket[id].price += shoe.price * shoe.quantity;
	});
	res.send(basket[id]);
});

router.get('/secure', function(req, res) {
	res.send({secret: 'Yellow'});
});

router.post('/user/signin', function(req, res) {
	if(req.body.user !== 'Paul' && req.body.pass !== 'Submarine')
		return res.status(401).send({error: 'Not authorised'});

	req.session.user = _.clone(req.body);
	res.status(200).send({});
});

router.delete('/user/signin', function(req, res) {
	req.session.destroy()
	res.status(200).send({});
});


app
	.use(bodyParser.json({limit: '70mb'}))
	.use(session({
		secret: 'you will never know',
		cookie:{maxAge: 10 * 1000},
		saveUninitialized: false,
		resave: true,
		rolling: true
	}))
	.use(function(req, res, next) {
		if (req.path !== '/secure')
			return next();

		if (!req.session.user)
			return res.status(401).send({error: 'Not authorised'});

		next();
	})
	.use(router)
	.listen(3000);

console.log('server is listening at 3000');