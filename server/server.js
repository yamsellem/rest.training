"use strict";

var express = require('express'),
	bodyParser = require('body-parser'),
	jwt = require('express-jwt'),
	_ = require('lodash'),
	app = express(),
	server = require('http').createServer(app),
	io = require('socket.io')(server),
	secret = require('./config').secret,
	checker = require('./checker');

app
	.use(express.static(__dirname + '/race'))
	.use(bodyParser.json({limit: '70mb'}))
	.use(jwt({ secret: secret })
	.unless(function(req) {
		return (_.contains(['/user/signin', '/user'], req.path) && req.method === 'POST')
			|| req.path.indexOf('/socket.io') !== -1;
	}))
	.use(function (err, req, res, next) {
		if (err.name === 'UnauthorizedError')
			res.status(401).send({error: 'Not authorized'});
	})
	.use(require('./user'));

checker.loop(io);

server.listen(3030);
console.log('server is listening at 3030');