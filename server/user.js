"use strict";

var _ = require('lodash'),
	router = require('express').Router(),
	jwt = require('jsonwebtoken'),
	secret = require('./config').secret;

var names = [];

router.get('/user', function(req, res) {
	res.send({ip: req.ip, id: 0}); // id for backbone
});

router.post('/user', function(req, res) {
	var name = req.body.name;
	if(_.isEmpty(name) || name.length < 3)
		return res.status(412).send({error: 'missing parameters'});

	name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
	if(_.contains(names, name))
		return res.status(412).send({error: 'Name already exists'});

	names.push(name);

	var token = jwt.sign({name: name, ip: req.ip}, secret, { expiresInMinutes: 60*5 });
	res.send({name: name, token: token, id: 0});
});

router.delete('/user', function(req, res) {
	names = _.filter(names, req.user.name)
	res.send({});
});

module.exports = router;